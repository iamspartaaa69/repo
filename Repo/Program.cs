﻿using System;

namespace Repo
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int a = 0;
            int total = 0;
            Console.WriteLine("Insert your number: ");
            int b = Convert.ToInt32(Console.ReadLine());
            while (a<b)
            {
                a++;
                Console.WriteLine(a);
                total += a;
            }
            Console.WriteLine("The sum is:" + total);
        }
    }
}
